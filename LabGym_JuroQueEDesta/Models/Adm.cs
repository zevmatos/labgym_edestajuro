﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Adm
    {
        public Adm()
        {
            Aulaslecionadas = new HashSet<Aulaslecionadas>();
            Exercicio = new HashSet<Exercicio>();
            Solicita = new HashSet<Solicita>();
            Utilizador = new HashSet<Utilizador>();
        }

        public int IdAdm { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Passwordd { get; set; }
        public int Telefone { get; set; }

        public virtual ICollection<Aulaslecionadas> Aulaslecionadas { get; set; }
        public virtual ICollection<Exercicio> Exercicio { get; set; }
        public virtual ICollection<Solicita> Solicita { get; set; }
        public virtual ICollection<Utilizador> Utilizador { get; set; }
    }
}
