﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class PlanoExer
    {
        public PlanoExer()
        {
            PlanoExercExercicio = new HashSet<PlanoExercExercicio>();
        }

        public int IdPlano { get; set; }
        public int IdProfCria { get; set; }
        public int IdSocio { get; set; }

        public virtual Prof IdProfCriaNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
        public virtual ICollection<PlanoExercExercicio> PlanoExercExercicio { get; set; }
    }
}
