﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Exercicio
    {
        public Exercicio()
        {
            PlanoExercExercicio = new HashSet<PlanoExercExercicio>();
        }

        public int IdExer { get; set; }
        public int IdAdminCria { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }
        public string Texto { get; set; }
        public string Video { get; set; }

        public virtual Adm IdAdminCriaNavigation { get; set; }
        public virtual ICollection<PlanoExercExercicio> PlanoExercExercicio { get; set; }
    }
}
