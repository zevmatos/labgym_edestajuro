﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Utilizador
    {
        public Utilizador()
        {
            MensagensIdEnviaNavigation = new HashSet<Mensagens>();
            MensagensIdRecebeNavigation = new HashSet<Mensagens>();
            Prof = new HashSet<Prof>();
            Socio = new HashSet<Socio>();
            
        
        }

       
        public int IdUti { get; set; }
        
        public int IdAdminCria { get; set; }
       
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public int Telefone { get; set; }

        [Required]
        public string Passwordd { get; set; }
        public string Foto { get; set; }

        [Required]
        public string Sexo { get; set; }

        public virtual Adm IdAdminCriaNavigation { get; set; }
        public virtual ICollection<Mensagens> MensagensIdEnviaNavigation { get; set; }
        public virtual ICollection<Mensagens> MensagensIdRecebeNavigation { get; set; }
        public virtual ICollection<Prof> Prof { get; set; }
        public virtual ICollection<Socio> Socio { get; set; }
    }
}
