﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Socio
    {
        public Socio()
        {
            InscreveSocio = new HashSet<InscreveSocio>();
            PlanoExer = new HashSet<PlanoExer>();
            RegPeso = new HashSet<RegPeso>();
            Solicita = new HashSet<Solicita>();
        }

        public int NUti { get; set; }
        public int IdSocio { get; set; }
        public int Altura { get; set; }

        public virtual Utilizador NUtiNavigation { get; set; }
        public virtual ICollection<InscreveSocio> InscreveSocio { get; set; }
        public virtual ICollection<PlanoExer> PlanoExer { get; set; }
        public virtual ICollection<RegPeso> RegPeso { get; set; }
        public virtual ICollection<Solicita> Solicita { get; set; }
    }
}
