﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Aulaslecionadas
    {
        public int IdAulasg { get; set; }
        public int CriadoPor { get; set; }
        public int IdProf { get; set; }
        public int Duracao { get; set; }
        public int Hora { get; set; }
        public string DiaSemana { get; set; }

        public virtual Adm CriadoPorNavigation { get; set; }
        public virtual Aulasgrupo IdAulasgNavigation { get; set; }
        public virtual Prof IdProfNavigation { get; set; }
    }
}
