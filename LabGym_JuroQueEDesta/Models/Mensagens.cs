﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Mensagens
    {
        public int IdMensagem { get; set; }
        public string Conteudo { get; set; }
        public int IdEnvia { get; set; }
        public int IdRecebe { get; set; }
        public bool Lida { get; set; }
        public bool Arquivada { get; set; }
        public DateTime DataEnvio { get; set; }

        public virtual Utilizador IdEnviaNavigation { get; set; }
        public virtual Utilizador IdRecebeNavigation { get; set; }
    }
}
