﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class PlanoExercExercicio
    {
        public int IdPlanoexerExer { get; set; }
        public int IdPlano { get; set; }
        public int IdExerc { get; set; }
        public int NRep { get; set; }
        public float Duracao { get; set; }
        public int Series { get; set; }
        public float DescansoSerie { get; set; }

        public virtual Exercicio IdExercNavigation { get; set; }
        public virtual PlanoExer IdPlanoNavigation { get; set; }
    }
}
