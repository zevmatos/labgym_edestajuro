﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Lab4gymContext : DbContext
    {
        public Lab4gymContext()
        {
        }

        public Lab4gymContext(DbContextOptions<Lab4gymContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Adm> Adm { get; set; }
        public virtual DbSet<Aulasgrupo> Aulasgrupo { get; set; }
        public virtual DbSet<Aulaslecionadas> Aulaslecionadas { get; set; }
        public virtual DbSet<Exercicio> Exercicio { get; set; }
        public virtual DbSet<InscreveSocio> InscreveSocio { get; set; }
        public virtual DbSet<Mensagens> Mensagens { get; set; }
        public virtual DbSet<PlanoExer> PlanoExer { get; set; }
        public virtual DbSet<PlanoExercExercicio> PlanoExercExercicio { get; set; }
        public virtual DbSet<Prof> Prof { get; set; }
        public virtual DbSet<RegPeso> RegPeso { get; set; }
        public virtual DbSet<Socio> Socio { get; set; }
        public virtual DbSet<Solicita> Solicita { get; set; }
        public virtual DbSet<Utilizador> Utilizador { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Lab4gym;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Adm>(entity =>
            {
                entity.HasKey(e => e.IdAdm)
                    .HasName("PK__adm__6BE8F80F56BC05ED");

                entity.ToTable("adm");

                entity.Property(e => e.IdAdm)
                    .HasColumnName("id_adm")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Passwordd)
                    .IsRequired()
                    .HasColumnName("passwordd")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Telefone).HasColumnName("telefone");
            });

            modelBuilder.Entity<Aulasgrupo>(entity =>
            {
                entity.HasKey(e => e.IdAulasg)
                    .HasName("PK__aulasgru__30CD5B0EE34AD641");

                entity.ToTable("aulasgrupo");

                entity.Property(e => e.IdAulasg)
                    .HasColumnName("id_aulasg")
                    .ValueGeneratedNever();

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Lotacao).HasColumnName("lotacao");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Aulaslecionadas>(entity =>
            {
                entity.HasKey(e => new { e.IdAulasg, e.IdProf })
                    .HasName("PK__aulaslec__F0176F8A579BE538");

                entity.ToTable("aulaslecionadas");

                entity.Property(e => e.IdAulasg).HasColumnName("id_aulasg");

                entity.Property(e => e.IdProf).HasColumnName("id_prof");

                entity.Property(e => e.CriadoPor).HasColumnName("criado_por");

                entity.Property(e => e.DiaSemana)
                    .IsRequired()
                    .HasColumnName("dia_semana")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Duracao).HasColumnName("duracao");

                entity.Property(e => e.Hora).HasColumnName("hora");

                entity.HasOne(d => d.CriadoPorNavigation)
                    .WithMany(p => p.Aulaslecionadas)
                    .HasForeignKey(d => d.CriadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aulasleci__criad__34C8D9D1");

                entity.HasOne(d => d.IdAulasgNavigation)
                    .WithMany(p => p.Aulaslecionadas)
                    .HasForeignKey(d => d.IdAulasg)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aulasleci__id_au__32E0915F");

                entity.HasOne(d => d.IdProfNavigation)
                    .WithMany(p => p.Aulaslecionadas)
                    .HasForeignKey(d => d.IdProf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aulasleci__id_pr__33D4B598");
            });

            modelBuilder.Entity<Exercicio>(entity =>
            {
                entity.HasKey(e => e.IdExer)
                    .HasName("PK__exercici__FA3D0777FD0BD991");

                entity.ToTable("exercicio");

                entity.Property(e => e.IdExer)
                    .HasColumnName("id_exer")
                    .ValueGeneratedNever();

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdminCria).HasColumnName("id_adminCria");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Video)
                    .IsRequired()
                    .HasColumnName("video")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdminCriaNavigation)
                    .WithMany(p => p.Exercicio)
                    .HasForeignKey(d => d.IdAdminCria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__exercicio__id_ad__300424B4");
            });

            modelBuilder.Entity<InscreveSocio>(entity =>
            {
                entity.HasKey(e => e.IdInscri)
                    .HasName("PK__inscreve__347B72EBE2D46E5F");

                entity.ToTable("inscreveSocio");

                entity.HasIndex(e => new { e.IdSocio, e.DataAula })
                    .HasName("UQ__inscreve__A4694B7C577C5ACC")
                    .IsUnique();

                entity.Property(e => e.IdInscri)
                    .HasColumnName("id_inscri")
                    .ValueGeneratedNever();

                entity.Property(e => e.DataAula).HasColumnType("smalldatetime");

                entity.Property(e => e.IdAula).HasColumnName("id_aula");

                entity.Property(e => e.IdSocio).HasColumnName("id_socio");

                entity.HasOne(d => d.IdAulaNavigation)
                    .WithMany(p => p.InscreveSocio)
                    .HasForeignKey(d => d.IdAula)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__inscreveS__id_au__3E52440B");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.InscreveSocio)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__inscreveS__id_so__3D5E1FD2");
            });

            modelBuilder.Entity<Mensagens>(entity =>
            {
                entity.HasKey(e => e.IdMensagem)
                    .HasName("PK__Mensagen__663A74B3BA553642");

                entity.Property(e => e.IdMensagem)
                    .HasColumnName("id_mensagem")
                    .ValueGeneratedNever();

                entity.Property(e => e.Arquivada).HasColumnName("arquivada");

                entity.Property(e => e.Conteudo)
                    .IsRequired()
                    .HasColumnName("conteudo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DataEnvio)
                    .HasColumnName("data_envio")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.IdEnvia).HasColumnName("id_envia");

                entity.Property(e => e.IdRecebe).HasColumnName("id_recebe");

                entity.Property(e => e.Lida).HasColumnName("lida");

                entity.HasOne(d => d.IdEnviaNavigation)
                    .WithMany(p => p.MensagensIdEnviaNavigation)
                    .HasForeignKey(d => d.IdEnvia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagens__id_en__48CFD27E");

                entity.HasOne(d => d.IdRecebeNavigation)
                    .WithMany(p => p.MensagensIdRecebeNavigation)
                    .HasForeignKey(d => d.IdRecebe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mensagens__id_re__49C3F6B7");
            });

            modelBuilder.Entity<PlanoExer>(entity =>
            {
                entity.HasKey(e => e.IdPlano)
                    .HasName("PK__PlanoExe__04D50D842B110FAF");

                entity.Property(e => e.IdPlano)
                    .HasColumnName("id_plano")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdProfCria).HasColumnName("id_profCria");

                entity.Property(e => e.IdSocio).HasColumnName("id_socio");

                entity.HasOne(d => d.IdProfCriaNavigation)
                    .WithMany(p => p.PlanoExer)
                    .HasForeignKey(d => d.IdProfCria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__id_pr__44FF419A");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.PlanoExer)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__id_so__45F365D3");
            });

            modelBuilder.Entity<PlanoExercExercicio>(entity =>
            {
                entity.HasKey(e => e.IdPlanoexerExer)
                    .HasName("PK__PlanoExe__477B89045AEEF2DD");

                entity.ToTable("PlanoExerc_Exercicio");

                entity.Property(e => e.IdPlanoexerExer)
                    .HasColumnName("id_planoexer_exer")
                    .ValueGeneratedNever();

                entity.Property(e => e.DescansoSerie).HasColumnName("descansoSerie");

                entity.Property(e => e.Duracao).HasColumnName("duracao");

                entity.Property(e => e.IdExerc).HasColumnName("id_exerc");

                entity.Property(e => e.IdPlano).HasColumnName("id_plano");

                entity.Property(e => e.NRep).HasColumnName("nRep");

                entity.Property(e => e.Series).HasColumnName("series");

                entity.HasOne(d => d.IdExercNavigation)
                    .WithMany(p => p.PlanoExercExercicio)
                    .HasForeignKey(d => d.IdExerc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__id_ex__4D94879B");

                entity.HasOne(d => d.IdPlanoNavigation)
                    .WithMany(p => p.PlanoExercExercicio)
                    .HasForeignKey(d => d.IdPlano)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanoExer__id_pl__4CA06362");
            });

            modelBuilder.Entity<Prof>(entity =>
            {
                entity.HasKey(e => e.IdProf)
                    .HasName("PK__Prof__0DA3484D0655DD67");

                entity.Property(e => e.IdProf)
                    .HasColumnName("id_prof")
                    .ValueGeneratedNever();

                entity.Property(e => e.Especial)
                    .IsRequired()
                    .HasColumnName("especial")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NUti).HasColumnName("n_uti");

                entity.HasOne(d => d.NUtiNavigation)
                    .WithMany(p => p.Prof)
                    .HasForeignKey(d => d.NUti)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Prof__n_uti__2B3F6F97");
            });

            modelBuilder.Entity<RegPeso>(entity =>
            {
                entity.HasKey(e => e.IdReg)
                    .HasName("PK__RegPeso__6ABE6F0C1170583B");

                entity.Property(e => e.IdReg).HasColumnName("id_reg");

                entity.Property(e => e.DataRegP).HasColumnType("smalldatetime");

                entity.Property(e => e.IdProf).HasColumnName("id_prof");

                entity.Property(e => e.IdSoc).HasColumnName("id_soc");

                entity.HasOne(d => d.IdProfNavigation)
                    .WithMany(p => p.RegPeso)
                    .HasForeignKey(d => d.IdProf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RegPeso__id_prof__4222D4EF");

                entity.HasOne(d => d.IdSocNavigation)
                    .WithMany(p => p.RegPeso)
                    .HasForeignKey(d => d.IdSoc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RegPeso__id_soc__412EB0B6");
            });

            modelBuilder.Entity<Socio>(entity =>
            {
                entity.HasKey(e => e.IdSocio)
                    .HasName("PK__Socio__3718538390A7585C");

                entity.Property(e => e.IdSocio)
                    .HasColumnName("id_socio")
                    .ValueGeneratedNever();

                entity.Property(e => e.Altura).HasColumnName("altura");

                entity.Property(e => e.NUti).HasColumnName("n_uti");

                entity.HasOne(d => d.NUtiNavigation)
                    .WithMany(p => p.Socio)
                    .HasForeignKey(d => d.NUti)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Socio__n_uti__286302EC");
            });

            modelBuilder.Entity<Solicita>(entity =>
            {
                entity.HasKey(e => e.IdSolicita)
                    .HasName("PK__solicita__B28E0332EE668882");

                entity.ToTable("solicita");

                entity.Property(e => e.IdSolicita)
                    .HasColumnName("id_solicita")
                    .ValueGeneratedNever();

                entity.Property(e => e.Dataa)
                    .HasColumnName("dataa")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.IdAdm).HasColumnName("id_adm");

                entity.Property(e => e.IdProf).HasColumnName("id_prof");

                entity.Property(e => e.IdSocio).HasColumnName("id_socio");

                entity.HasOne(d => d.IdAdmNavigation)
                    .WithMany(p => p.Solicita)
                    .HasForeignKey(d => d.IdAdm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__solicita__id_adm__38996AB5");

                entity.HasOne(d => d.IdProfNavigation)
                    .WithMany(p => p.Solicita)
                    .HasForeignKey(d => d.IdProf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__solicita__id_pro__398D8EEE");

                entity.HasOne(d => d.IdSocioNavigation)
                    .WithMany(p => p.Solicita)
                    .HasForeignKey(d => d.IdSocio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__solicita__id_soc__37A5467C");
            });

            modelBuilder.Entity<Utilizador>(entity =>
            {
                entity.HasKey(e => e.IdUti)
                    .HasName("PK__Utilizad__6AE817D415D3747B");

                entity.Property(e => e.IdUti)
                    .HasColumnName("id_uti")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdminCria).HasColumnName("id_AdminCria");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Passwordd)
                    .IsRequired()
                    .HasColumnName("passwordd")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasColumnName("sexo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Telefone).HasColumnName("telefone");

                entity.HasOne(d => d.IdAdminCriaNavigation)
                    .WithMany(p => p.Utilizador)
                    .HasForeignKey(d => d.IdAdminCria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Utilizado__id_Ad__25869641");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
