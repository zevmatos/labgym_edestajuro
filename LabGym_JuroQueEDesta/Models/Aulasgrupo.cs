﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Aulasgrupo
    {
        public Aulasgrupo()
        {
            Aulaslecionadas = new HashSet<Aulaslecionadas>();
            InscreveSocio = new HashSet<InscreveSocio>();
        }

        public int IdAulasg { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }
        public string Texto { get; set; }
        public int Lotacao { get; set; }

        public virtual ICollection<Aulaslecionadas> Aulaslecionadas { get; set; }
        public virtual ICollection<InscreveSocio> InscreveSocio { get; set; }
    }
}
