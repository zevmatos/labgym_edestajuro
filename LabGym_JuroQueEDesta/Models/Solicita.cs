﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Solicita
    {
        public int IdSolicita { get; set; }
        public int IdSocio { get; set; }
        public int IdProf { get; set; }
        public int IdAdm { get; set; }
        public DateTime Dataa { get; set; }
        public bool Estado { get; set; }

        public virtual Adm IdAdmNavigation { get; set; }
        public virtual Prof IdProfNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
    }
}
