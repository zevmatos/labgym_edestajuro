﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class Prof
    {
        public Prof()
        {
            Aulaslecionadas = new HashSet<Aulaslecionadas>();
            PlanoExer = new HashSet<PlanoExer>();
            RegPeso = new HashSet<RegPeso>();
            Solicita = new HashSet<Solicita>();
        }

        public int NUti { get; set; }
        public int IdProf { get; set; }
        public string Especial { get; set; }

        public virtual Utilizador NUtiNavigation { get; set; }
        public virtual ICollection<Aulaslecionadas> Aulaslecionadas { get; set; }
        public virtual ICollection<PlanoExer> PlanoExer { get; set; }
        public virtual ICollection<RegPeso> RegPeso { get; set; }
        public virtual ICollection<Solicita> Solicita { get; set; }
    }
}
