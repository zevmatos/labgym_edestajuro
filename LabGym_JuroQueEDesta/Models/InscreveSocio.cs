﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class InscreveSocio
    {
        public DateTime DataAula { get; set; }
        public int IdInscri { get; set; }
        public int IdSocio { get; set; }
        public int IdAula { get; set; }

        public virtual Aulasgrupo IdAulaNavigation { get; set; }
        public virtual Socio IdSocioNavigation { get; set; }
    }
}
