﻿using System;
using System.Collections.Generic;

namespace LabGym_JuroQueEDesta.Models
{
    public partial class RegPeso
    {
        public DateTime DataRegP { get; set; }
        public float Peso { get; set; }
        public int IdReg { get; set; }
        public int IdSoc { get; set; }
        public int IdProf { get; set; }

        public virtual Prof IdProfNavigation { get; set; }
        public virtual Socio IdSocNavigation { get; set; }
    }
}
